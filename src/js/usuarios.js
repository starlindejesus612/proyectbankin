import { saveUser } from "./conection.js";


const nameUser = document.getElementById("username");
const lastname = document.getElementById("lastname");
const email = document.getElementById("Email");
const password = document.getElementById("password");
const celular = document.getElementById("celular");
const telefono =document.getElementById("Telefono");
const btn = document.getElementById("button");

/**Creamos un arreglo con todos los elemantes para evitar que guarden campos vacios, es necesario que todos este llenos */
const elements = [nameUser,lastname,email,password,celular]

/**NumberCard nos retornara el numero de tarjeta del usuario  */
const NumberCard = () => `00${Math.floor(Math.random()*100)}-${Math.floor(Math.random()*100) + 1000}-${Math.floor(Math.random()*100)+ Math.floor(Math.random()*100) + 100}`

/**expresion regular */
const regex = (text)=>{
    const EmailRegex = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/gi
    return EmailRegex.test(text)

}
const saveDatos = () =>{
    /**Numero de la tarjeta */
    const tarjeta = NumberCard()

    /**Creamos el objeto usuario para guardarlo en la base de datos IndexDB */
    const user = {
        nombre: nameUser.value,
        apellido: lastname.value,
        email:email.value,
        password:password.value,
        celular:celular.value,
        telefono:telefono.value,
        card:tarjeta,
        monto:500,
        cuenta:[]
    }
    /**La base de datos necesita una llave, utilizamos el email y pasword nos servira para el login */
    const key = {
        email:email.value
    }

    /** */
   saveUser(user,key)
   console.log(user)
   console.log(key)

    Swal.fire({
        title: 'Datos Guardados',
        text: `Usuario registrado el numero de su tarjeta es : ${tarjeta}, el monto de cuenta es 500$`,
        icon: 'success',
        confirmButtonText: 'Okay'
      })
}
const saveError = (text) =>{
    console.log("hay un error")
    Swal.fire({
        title: 'Error!',
        text: text,
        icon: 'error',
        confirmButtonText: 'Okay'
      })
}


btn.addEventListener("click",(e)=>{
    e.preventDefault();
    /**Evaluamos que todo este lleno, utilizamos el metodo map() para buscar algun input vacio */
   const data =  elements.map((element)=>{
        if ((element.value.length === 0) || (element.value.length>50))return false
        else return true
    })
    /**En caso de que un input este vacio utilizaremos la funcion saveError, de lo contrario todo esta lleno usaremos saveDatos */
    /**El metodo include del arreglo se encargara de buscar por lo menos un input vacio, si existe uno vacio en devolvemos la funcion saveError */
     data.includes(false) ? saveError : saveDatos;
    if(data.includes(false)){
        saveError('Debe llenar todo los campos del formulario para poeder registrarse')
    }else{
        if(regex(email.value)){
            saveDatos()
        }else{
            saveError("debe ingresar un correo electronico valido")
        }
    }
   
  
    
})