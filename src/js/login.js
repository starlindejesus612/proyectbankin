

const email = document.getElementById("user")
const password = document.getElementById("password")
// const form = document.getElementById("btn")

const alerta = (text) => {
    Swal.fire({
        title: 'Error',
        text:text,
        icon: 'error',
        confirmButtonText: 'Okay'
      })
}

btn.addEventListener("click",(e)=>{
    const correo = email.value
    const userDatos = localStorage.getItem(correo);
    const user = JSON.parse(userDatos)
    if(user){
        if(user.password === password.value){
            document.cookie = `cuenta=${correo}`
            // console.log(cuenta.getNumber())
            location.assign("/src/view/panel.html")
        }else{
            alerta("Contraseña incorrecta, por favor intente de nuevo!")
        }
    }else{
        alerta("El usuario no existe, por favor cree una cuenta")
    }

})
