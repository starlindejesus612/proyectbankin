import { readUser, saveUser } from "./conection.js"


/***Recuperamos los datos del usuario logueado que se encuentra en la cookie, y posterior buscar en el localStorage */
const cuenta = decodeURIComponent(document.cookie)
const NumberCard = cuenta.split("=")[1]

// const datos = localStorage.getItem(NumberCard)
const getUser = () => readUser(NumberCard)
let user = readUser(NumberCard)
const actualizar = () =>{
    const balc = document.getElementById("balance")
    balc.children.title.textContent = user.monto;
    // headeTitle.textContent = "Bienvenido " + user.nombre
    let dolar = parseFloat(user.monto) / 58
    document.getElementById("dolarCant").textContent = " ,dolares de esta cuenta :" + Math.floor(dolar)+"$";
}


let fecha = new Date()
const history = {
    monto:"",
    pago:"",
    fechaDepago:"",
    tipo:""
}




/**Para actualizar los montos */
actualizar()
/**Los modales */
const modalDepositar = document.getElementById("depositarModal")
const modalPeso = document.getElementById("modalPeso")
const modalDolares = document.getElementById("modalDolares")
/**Para abrir el modal */
const btnDepositar = document.getElementById("btnDepositar")
const btnPeso = document.getElementById("btnPeso")
const btnDolares = document.getElementById("btnDolares")

/**Botones para cerrar el modal */
const btnCerrar = document.getElementById("cerrar")
const btcerrarPeso = document.getElementById("btcerrarPeso")
const btnCerrarDolares = document.getElementById("btnCerrarDolares")


btnDepositar.addEventListener("click",e=>{
    e.preventDefault()
    modalDepositar.classList.replace("hidden","visible")
})
btnPeso.addEventListener("click",e=>{
    e.preventDefault()
    modalPeso.classList.replace("hidden","visible")
})
btnDolares.addEventListener("click",e=>{
    e.preventDefault()
    modalDolares.classList.replace("hidden","visible")
})
btnCerrar.addEventListener("click",e=>{
    modalDepositar.classList.replace("visible","hidden")
   
})
btcerrarPeso.addEventListener("click",e=>{
    modalPeso.classList.replace("visible","hidden")
   
})
btnCerrarDolares.addEventListener("click",e=>{
    modalDolares.classList.replace("visible","hidden")
  
})


/** */
const depositar = (monto, ingres) =>parseFloat(monto)+parseFloat(ingres)
const pagar = (monto, ingres) =>parseFloat(monto)-parseFloat(ingres)
const alerta = (title,text,icon)=>{
    Swal.fire({
        title: title,
        text: text,
        icon: icon,
        confirmButtonText: 'Okay'
      })
}
const monto = document.getElementById("monto")
const peso = document.getElementById("peso")
const dolares =  document.getElementById("dolares")

monto.addEventListener("submit",e=>{
    e.preventDefault()
    if(e.target[0].value){
        if(e.target[0].value<9999){
            user.monto = depositar(user.monto,e.target[0].value)
          
            history.fechaDepago  =`${fecha.getDay()}/${fecha.getMonth()}`
            history.monto = user.monto;
            history.pago =e.target[0].value;
            history.tipo = "monto"
            user.cuenta.push(history)
            localStorage.setItem(NumberCard, JSON.stringify(user))
            localStorage.getItem(NumberCard)
            actualizar()
            alerta("monto aplicado","Pago realizado con exito",'success')
            e.target[0].value =" "
        }else{
            alerta("Alerta","Error de trasferencia comuniquese con el banco",'error')
            e.target[0].value =" "
        }
    }
})
peso.addEventListener("submit",e=>{
    e.preventDefault()
    if(e.target[0].value){
        if(e.target[0].value<10000){
            if((user.monto>e.target[0].value) && (e.target[0].value>0) ){
                user.monto = pagar(user.monto,e.target[0].value)
                // saveUser(user,NumberCard)
                history.fechaDepago  =`${fecha.getDay()}/${fecha.getMonth()}`
                history.monto = user.monto;
                history.pago =e.target[0].value;
                history.tipo = "peso"
                user.cuenta.push(history)
                localStorage.setItem(NumberCard, JSON.stringify(user))
                localStorage.getItem(NumberCard)
                actualizar()
                alerta("pago realizado","Pago de la cuenta realizado con exito con exito","success")
                e.target[0].value =" "
            }else{
                alerta("error!","No tiene monto suficiente para pagar","error")
                e.target[0].value =" "
            }
        }else{
            alerta("Error!","Error de transacción!","error")
            e.target[0].value =" "
        }
    }
})
const convertoPeso = (dolar) =>dolar*58
dolares.addEventListener("submit",e=>{
    e.preventDefault()
    let dolar = parseFloat(user.monto) / 58
    if(e.target[0].value){
        if((dolar>e.target[0].value) && (e.target[0].value>0)){
            user.monto = pagar(user.monto,convertoPeso(e.target[0].value))
            // saveUser(user,NumberCard)
            history.fechaDepago  =`${fecha.getDay()}/${fecha.getMonth()}`
            history.monto = user.monto / 58 ;
            history.pago =convertoPeso(e.target[0].value);
            history.tipo = "dolar"
            user.cuenta.push(history)
            localStorage.setItem(NumberCard, JSON.stringify(user))
            localStorage.getItem(NumberCard)
            actualizar()
            alerta("Pago de la cuenta realizado con exito con exito")
            e.target[0].value =" "
        }else{
            alerta("error!","No tiene monto suficiente para pagar","error")
            e.target[0].value =" "
        }
    }
})



const item = document.getElementById("list")
item.lastElementChild.addEventListener("click",()=>{
    alerta("Informacion","Contáctenos a 809-687-APAP (2727,  sin cargos desde EEUU 1-877-589-2727, comuníquese por cualquier error en la plataforma","info")
})

const count = document.getElementById("count")

count.addEventListener("click",()=>{
    document.getElementById("menu").classList.replace("hidden","visible")
})
document.getElementById("btnMenu").addEventListener("click",()=>{
    document.getElementById("menu").classList.replace("visible","hidden")
})